FROM alpine:latest

COPY github_key .

RUN apk update && apk add curl git bash htop nodejs npm openssh

RUN eval $(ssh-agent) &&\
  ssh-add github_key &&\
  ssh-keyscan -H github.com >> /etc/ssh/ssh_known_hosts &&\
  git clone git@github.com:jonstump/book-binder.git /book-binder

WORKDIR book-binder

RUN npm install &&\
  npm audit fix &&\
  npm run build

EXPOSE 3000

RUN ng serve
